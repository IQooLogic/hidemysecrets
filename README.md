```
Usage: hide-my-secret [command] [command options]
  Commands:
    embed      Embed message to image
      Usage: embed [options]
        Options:
          --destination, -d
            Path to resulting image file
            Default: image_with_message.png
        * --image, -i
            Path to image file
        * --message, -m
            Path to message file

    extract      Extract message from image
      Usage: extract [options]
        Options:
          --destination, -d
            Path to extracted message file
            Default: extracted_message.txt
        * --image, -i
            Path to image file with message
```
