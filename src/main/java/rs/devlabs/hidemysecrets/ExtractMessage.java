package rs.devlabs.hidemysecrets;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import org.slf4j.LoggerFactory;
import static rs.devlabs.hidemysecrets.App.CHARSET;
import static rs.devlabs.hidemysecrets.App.SEPARATOR;

/**
 *
 * @author milos
 */
@Parameters(commandDescription = "Extract message from image")
public class ExtractMessage implements Runnable {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(App.class);
    @Parameter(names = {"--image", "-i"}, description = "Path to image file with message", required = true)
    private String image;
    @Parameter(names = {"--destination", "-d"}, description = "Path to extracted message file", required = false)
    private String destination = "extracted_message.txt";
    
    @Override
    public void run() {
        try {
            extractMessage(image, destination);
        } catch (IOException ex) {
            LOGGER.error("Error while extracting message", ex);
        }
    }

    private void extractMessage(String image, String destination) throws FileNotFoundException, IOException {
        try (FileInputStream fis = new FileInputStream(image);
                InputStreamReader isr = new InputStreamReader(fis, Charset.forName(CHARSET))) {
            try (BufferedReader in = new BufferedReader(isr);
                    FileOutputStream fos = new FileOutputStream(destination);
                    Writer out = new BufferedWriter(new OutputStreamWriter(fos, Charset.forName(CHARSET)))) {
                boolean foundMessage = false;
                String line;
                while ((line = in.readLine()) != null) {
                    if (foundMessage) {
                        out.write(line);
                        out.write(System.lineSeparator());
                    }
                    if (line.equalsIgnoreCase(SEPARATOR) && !foundMessage) {
                        foundMessage = true;
                    }
                }
            }
        }
    }
}
