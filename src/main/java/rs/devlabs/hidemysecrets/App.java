package rs.devlabs.hidemysecrets;

import com.beust.jcommander.JCommander;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IQooLogic
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    public static final String SEPARATOR = "##############################################";
    public static final String CHARSET = "UTF-8";
    
    // ./hide-my-secret embed
    //      -i /home/milos/NetBeansProjects/hide-my-secrets/image.png
    //      -m /home/milos/NetBeansProjects/hide-my-secrets/message.txt
    
    // ./hide-my-secret extract -i image_with_message.png
    public static void main(String[] args) {
        App app = new App(args);
    }
    
    private App(String[] args) {
        Map<String, Runnable> commands = new HashMap<>();

        JCommander jCommander = new JCommander(this);

        commands.put("embed", new EmbedMessage());
        commands.put("extract", new ExtractMessage());

        List<String> keys = new ArrayList<>(commands.keySet());
        Collections.sort(keys);
        keys.forEach((cmd) -> {
            jCommander.addCommand(cmd, commands.get(cmd));
        });

        try {
            jCommander.parse(args);
            LOGGER.info("Parsed command line");

            String cmd = jCommander.getParsedCommand();
            if (cmd == null) {
                jCommander.usage();
            } else {
                commands.get(cmd).run();
            }
        } catch (Exception ex) {
            LOGGER.error(null, ex);
        } finally {
            System.exit(0);
        }
    }
}
