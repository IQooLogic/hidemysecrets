package rs.devlabs.hidemysecrets;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import static rs.devlabs.hidemysecrets.App.SEPARATOR;

/**
 *
 * @author milos
 */
@Parameters(commandDescription = "Embed message to image")
public class EmbedMessage implements Runnable {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(App.class);
    @Parameter(names = {"--image", "-i"}, description = "Path to image file", required = true)
    private String image;
    @Parameter(names = {"--message", "-m"}, description = "Path to message file", required = true)
    private String message;
    @Parameter(names = {"--destination", "-d"}, description = "Path to resulting image file", required = false)
    private String destination = "image_with_message.png";

    @Override
    public void run() {
        try {
            embedMessage(image, message, destination);
        } catch (IOException ex) {
            LOGGER.error("Error while embeding message", ex);
        }
    }

    private void embedMessage(String image, String message, String destination) throws FileNotFoundException, IOException {
        byte[] newLineBytes = System.lineSeparator().getBytes();
        byte[] separatorBytes = SEPARATOR.getBytes();

        try (FileOutputStream fos = new FileOutputStream(destination)) {
            int i;
            try (FileInputStream fis1 = new FileInputStream(image)) {
                while ((i = fis1.read()) != -1) {
                    fos.write(i);
                }
            }

            fos.write(newLineBytes);
            fos.write(separatorBytes);
            fos.write(newLineBytes);

            try (FileInputStream fis2 = new FileInputStream(message)) {
                while ((i = fis2.read()) != -1) {
                    fos.write(i);
                }
            }
        }
    }
}
